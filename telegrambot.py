#!/usr/bin/python3

from telegram.ext import Updater, CommandHandler, MessageHandler
import socket
import pkpscraper

def bot_start(bot, update):
	update.message.reply_text("Wszystko działa.")

def bot_help(bot, update):
	update.message.reply_text("/help - wypisuje tą wiadomość \n/status - wyświetla aktualny status serwera \n/pkpdefault - wypisuje odjazdy, ustawienia domyślne \n/id [stacja] - wyszukuje i wypisuje ID podanej stacji \n/idraw [stacja] - zwraca surowy wynik wyszukiwania stacji \n/urlraw [stacja_startowa] [stacja_docelowa] [czas] [data] - zwraca link wykorzystany do wyszukiwania \n/d [stacja_startowa] [stacja_doecelowa] [czas] [data] - wypisuje najbliższe odjazdy ze stacji startowej do docelowej danego dnia o podanej godzinie \n/a [stacja_startowa] [stacja_docelowa] [czas] [data] - wypisuje godziny przyjazdu do stacji docelowej ze startowej, jeśli wyjechać o podanym czasie danego dnia \n\nBOT NIE PRZYJMUJE POLSKICH ZNAKÓW \nDla komend '/d', '/a' i '/urlraw': \n  -data powinna być podawana w formacie 'dd.mm.rr' \n  -czas powinien być podawany w formacie 'HH:MM'. \n  -jako zaktualizowana data działa 'd' \n  -jako zaktualizowany czas działa 't' \n  -aby jako stacji bezpośrednio użyć ID, poprzedź ID znakiem '-' \n  -wyszukując stacje spacje w ich nazwach zamień na kropki ('.')")

def bot_status(bot, update):
	update.message.reply_text("Telegram-bot działa na serwerze @"+socket.gethostname())
	update.message.reply_text("Aktualna data na serwerze: "+pkp.update_date())
	update.message.reply_text("Aktualny czas na serwerze: "+pkp.update_time())

def bot_pkpdefault(bot, update):
	# personalize that part
	pkp.prepare(pkp.update_date(), pkp.update_time(), pkp.station.by_alias("r"), pkp.station.by_alias("l"))
	update.message.reply_text("Najbliższe odjazdy ze stacji ... do stacji ...:")
	for element in pkp.depart_times():
		update.message.reply_text(element)

def bot_id(bot, update, args):
	update.message.reply_text(pkp.station.find_ID(" ".join(args.split("."))))

def bot_idraw(bot, update, args):
	update.message.reply_text(pkp.station.find_raw(" ".join(args.split("."))))

def bot_urlraw(bot, update, args):
	da_prepare_prepare(args[0], args[1], args[2], args[3])
	update.message.reply_text(pkp.request_url)

def da_prepare_prepare(a, b, t, d):
	if d == "d": d = pkp.update_date()
	if t == "t": t = pkp.update_time()
	if a[0] == '-': a = a[1:]
	else: a = pkp.station.by_alias(a.replace(".", " "))
	if b[0] == '-': b = b[1:]
	else: b = pkp.station.by_alias(b.replace(".", " "))
	pkp.prepare(a, b, t, d)

def bot_d(bot, update, args):
	da_prepare_prepare(args[0], args[1], args[2], args[3])
	for element in pkp.depart_times():
		update.message.reply_text(element)

def bot_a(bot, update, args):
	da_prepare_prepare(args[0], args[1], args[2], args[3])
	for element in pkp.arival_times():
		update.message.reply_text(element)

def bot():
	updater = Updater("") # fill with a bot API token
	updater.dispatcher.add_handler(CommandHandler("start", bot_start))
	updater.dispatcher.add_handler(CommandHandler("help", bot_help))
	updater.dispatcher.add_handler(CommandHandler("status", bot_status))
	updater.dispatcher.add_handler(CommandHandler("pkpdefault", bot_pkpdefault))
	updater.dispatcher.add_handler(CommandHandler("id", bot_id, pass_args=True))
	updater.dispatcher.add_handler(CommandHandler("idraw", bot_idraw, pass_args=True))
	updater.dispatcher.add_handler(CommandHandler("urlraw", bot_urlraw, pass_args=True))
	updater.dispatcher.add_handler(CommandHandler("d", bot_d, pass_args=True))
	updater.dispatcher.add_handler(CommandHandler("a", bot_a, pass_args=True))
	updater.start_polling()
	updater.idle()

if __name__ == '__main__':
	pkp = pkpscraper.PKP()
	bot()
