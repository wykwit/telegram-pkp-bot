#!/usr/bin/python3

import urllib.request
import time

class PKP:
	template_url = "https://rozklad-pkp.pl/pl/tp?REQ0JourneyStopsS0A=1&REQ0JourneyStopsS0G=POCZSTACJA&REQ0JourneyStopsZ0A=1&REQ0JourneyStopsZ0G=KONCSTACJA&REQ0JourneyDate=ODJAZDUDATA&REQ0JourneyTime=ODJAZDUCZAS&start=start"

	def __init__(self):
		self.update_time()
		self.update_date()
		self.station = self.Station()

	def update_time(self):
		self.current_time = time.strftime("%H:%M")
		return self.current_time

	def update_date(self):
		self.current_date = time.strftime("%d.%m.%y")
		return self.current_date

	def prepare(self, depart_station, arival_station, depart_time=None, depart_date=None):
		if not depart_time: depart_time = self.update_time()
		if not depart_date: depart_date = self.update_date()
		self.request_url = self.template_url
		self.request_url = self.request_url.replace("POCZSTACJA", depart_station)
		self.request_url = self.request_url.replace("KONCSTACJA", arival_station)
		self.request_url = self.request_url.replace("ODJAZDUCZAS", depart_time)
		self.request_url = self.request_url.replace("ODJAZDUDATA", depart_date)
		return self.update_content()

	def update_content(self):
		request_response = urllib.request.urlopen(self.request_url)
		self.response_content = str(request_response.read())
		return self.response_content

	def depart_times(self):
		found = 0
		search_query = "ODJAZD "
		result_length = (len(search_query)+5)
		results = []
		while found != result_length-1:
			found = self.response_content.find(search_query, found, len(self.response_content))+result_length
			a_result = self.response_content[found-result_length:found]
			if a_result[result_length-3:result_length-2] != ":" : continue;
			if not results or results[-1] != a_result :
				results.append(a_result)
		for i in range(0, len(results)):
			results[i] = results[i][len(search_query):result_length]
		return results

	def arival_times(self):
		found = 0
		search_query = "PRZYJAZD</span><span>"
		result_length = (len(search_query)+5)
		results = []
		while found != result_length-1:
			found = self.response_content.find(search_query, found, len(self.response_content))+result_length
			a_result = self.response_content[found-result_length:found]
			if a_result[result_length-3:result_length-2] != ":" : continue;
			if not results or results[-1] != a_result :
				results.append(a_result)
		for i in range(0, len(results)):
			results[i] = results[i][len(search_query):result_length]
		return results

	def find(self, depart_alias, arival_alias):
		self.prepare(self.station.by_alias(depart_alias), self.station.by_alias(arival_alias))
		return (self.depart_times(), self.arival_times())

	class Station:
		template_url = "https://rozklad-pkp.pl/station/search?term=WYSZUKIWANIE&short=0"
		ID_piaseczno = "5102694"
		ID_srodmiescie = "5100246"
		ID_sluzewiec = "5104157"

		def find_raw(self, search_query):
			search_query = search_query.replace(" ", "+")
			self.request_url = self.template_url
			self.request_url = self.request_url.replace("WYSZUKIWANIE", search_query)
			request_response = urllib.request.urlopen(self.request_url)
			self.response_content = str(request_response.read())
			return self.response_content

		def find_ID(self, search_query):
			self.find_raw(search_query)
			found_beg = self.response_content.find('"value":"')+len('"value":"')
			found_end = self.response_content.find('"', found_beg, len(self.response_content))
			return self.response_content[found_beg:found_end]

		def by_alias(self, search_query):
			if search_query=="p":
				return self.ID_piaseczno
			elif search_query=="r":
				return self.ID_srodmiescie
			elif search_query=="l":
				return self.ID_sluzewiec
			else:
				return self.find_ID(search_query)


if __name__ == "__main__":
	print("test...\n")
	pkp = PKP()
	date_str = "09.06.21"
	time_str = "12:57"
	print("Odjazdy ze Śródmieścia na Służewiec {} o {}:".format(date_str, time_str))
	pkp.prepare(pkp.station.by_alias("r"), pkp.station.by_alias("l"), time_str, date_str)
	print(pkp.depart_times())
	print("\nID stacji 'Zamość': ", pkp.station.find_ID("Zamosc"))
	print("\nPołączenia Bielsko - Służewiec, teraz:")
	pkp.prepare(pkp.station.by_alias("Bielsko"), pkp.station.by_alias("l"))
	print("Odjazdy:\t", pkp.depart_times())
	print("Przyjazdy:\t", pkp.arival_times())
	print("\nPołączenia Piaseczno - Śródmieście, teraz:")
	a, d = pkp.find(pkp.station.by_alias("p"), pkp.station.by_alias("l"))
	print("Odjazdy:\t", a)
	print("Przyjazdy:\t", d)

#pkpscraper by WikWG9, 09.06.2018r.
#license WTFPL, no rights reserved
